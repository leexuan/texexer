cmd.exe下执行：
	xelatex -shell-escape EI.tex

即可在EI.tex文件中\tikzexternalize[prefix=figures/]指定目录下生成对应图片文件，该图片名称由\tikzsetnextfilename{name}指定【注意：name中不能出现“.”,不能与该pgf图TeX代码文件名相同】

利用Sublime Text 3编译，修改如下配置：
1) Preferences->Package Setting->LaTeXTools->Settings - User: 
“builder_settings”:{
	// Other configuration
	"options": ["--shell-escape"], //添加的配置
	// Other configuration
}}

2)ctrl+shift+B:选择LaTeX - Basic Builder - XeLaTex